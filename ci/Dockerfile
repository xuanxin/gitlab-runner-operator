FROM golang:1.20.6-bullseye

RUN apt-get update -yq && \
    apt-get install -yq \
    locales \
    make \
    git \
    curl \
    python3-pip \
    python3 \
    ca-certificates \
    hub \
    skopeo && \
    rm -rf /var/lib/apt/lists/*

# Install Docker
RUN curl -Lo docker.tgz https://download.docker.com/linux/static/stable/x86_64/docker-20.10.14.tgz && \
    tar -zxvf docker.tgz && \
    mv docker/docker /usr/local/bin/docker && \
    chmod +x /usr/local/bin/docker && \
    rm docker.tgz && \
    rm -r docker

# Install docker buildx plugin
RUN mkdir -p ~/.docker/cli-plugins && \
    wget -q https://github.com/docker/buildx/releases/download/v0.7.1/buildx-v0.7.1.linux-amd64 -O ~/.docker/cli-plugins/docker-buildx && \
    chmod a+x ~/.docker/cli-plugins/docker-buildx

# Install envsubst https://github.com/a8m/envsubst
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin

ADD ci/commit-template /commit-template
ADD ci/create_github_pr.sh /create_github_pr.sh