package runner

import (
	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
)

// Controller is the Runner controller providing methods to build the relevant kubernetes resources
type Controller struct {
	cr *gitlabv1beta2.Runner

	imageResolver imageResolver
	// DEPRECATED: Should be removed when the `registration-token` is deprecated
	IsAuthToken bool
}

// New creates a new instance of Controller from a Runner instance
func New(cr *gitlabv1beta2.Runner, isAuthToken bool) *Controller {
	return &Controller{
		cr:            cr,
		imageResolver: &releaseDirImageResolver{},
		// DEPRECATED: Should be removed when the `registration-token` is deprecated
		IsAuthToken: isAuthToken,
	}
}
